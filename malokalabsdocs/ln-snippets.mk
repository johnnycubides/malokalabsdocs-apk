images-ln:
	# carta-chicas
	rm -rf docs/intro/kit/carta-chicas-img
	ln -sr snippets/carta-chicas/img docs/intro/kit/carta-chicas-img
	# leds
	rm -rf docs/biblioteca-electronica/leds/img
	ln -sr snippets/biblioteca-electronica/leds/img docs/biblioteca-electronica/leds/img
	# resistencia
	rm -rf docs/biblioteca-electronica/resistencia/img
	ln -sr snippets/biblioteca-electronica/resistencias/img docs/biblioteca-electronica/resistencia/img
	# protoboard
	rm -rf docs/biblioteca-electronica/protoboard/img
	ln -sr snippets/biblioteca-electronica/protoboard/img docs/biblioteca-electronica/protoboard/img
	# elementos-circuito
	rm -rf docs/biblioteca-electronica/elementos-circuitos/img
	ln -sr snippets/biblioteca-electronica/elementos-circuitos/img docs/biblioteca-electronica/elementos-circuitos/img
	# primer-circuito
	rm -rf docs/intro/mi-primer-circuito/primer-circuito-img
	ln -sr snippets/primer-circuito/img docs/intro/mi-primer-circuito/primer-circuito-img
	# alarma
	rm -rf docs/cacharros/alarma/img
	ln -sr snippets/cacharros/alarma/img docs/cacharros/alarma/img
	# theremin
	rm -rf docs/cacharros/theremin/img
	ln -sr snippets/cacharros/theremin/img docs/cacharros/theremin/img
	# baston_ultrasonido
	rm -rf docs/cacharros/baston_ultrasonido/img
	ln -sr snippets/cacharros/baston_ultrasonido/img docs/cacharros/baston_ultrasonido/img
	# codigo_morse
	rm -rf docs/cacharros/codigo_morse/img
	ln -sr snippets/cacharros/codigo_morse/img docs/cacharros/codigo_morse/img
	# bufanda
	rm -rf docs/cacharros/bufanda/img
	ln -sr snippets/cacharros/bufanda/img docs/cacharros/bufanda/img
	# detector_sed_1
	rm -rf docs/cacharros/detector_sed_1/img
	ln -sr snippets/cacharros/detector_sed_1/img docs/cacharros/detector_sed_1/img
	# detector_sed
	rm -rf docs/cacharros/detector_sed/img
	ln -sr snippets/cacharros/detector_sed/img docs/cacharros/detector_sed/img
	# hackeando_juguete
	rm -rf docs/cacharros/hackeando_juguete/img
	ln -sr snippets/cacharros/hackeando_juguete/img docs/cacharros/hackeando_juguete/img
	# humedad_suelo
	rm -rf docs/cacharros/humedad_suelo/img
	ln -sr snippets/cacharros/humedad_suelo/img docs/cacharros/humedad_suelo/img
	# reloj_binario
	rm -rf docs/cacharros/reloj_binario/img
	ln -sr snippets/cacharros/reloj_binario/img docs/cacharros/reloj_binario/img
	# semaforo
	rm -rf docs/cacharros/semaforo/img
	ln -sr snippets/cacharros/semaforo/img docs/cacharros/semaforo/img
