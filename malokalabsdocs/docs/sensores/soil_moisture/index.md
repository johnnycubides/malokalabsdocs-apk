[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Sensor de humedad del suelo

## Ejemplo de conexión

![Esquema humedad del suelo](nodemcu_humedad_suelo_bb.png)

## Ejemplo de programa

### Ejemplo de programación 1

A continuación te presentamos dos maneras de escribir el mismo algoritmo.

En este algoritmo usarás las nociones de la ecuación de la recta para parametrizar el sensor.

![Humedad del suelo](soil_moisture.png)

### Ejemplo de programación 2

Como en el reto relacionado a mi primera página web, deberás ver el resultado en el navegador de la tablet o celular.
Sino recuerdas como se realiza esta prueba, repasa el reto relacionado.

En éste algoritmo usamos la función de *cambiar rango* donde ella automáticamente determinará
cual será el valor de la medición en la nueva escala.

![algoritmo 2](suelo-algoritmo.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Prueba del sensor

Como en el reto relacionado a mi primera página web, deberás ver el resultado en el navegador de la tablet o celular.
Sino recuerdas como se realiza esta prueba, repasa el reto relacionado.
