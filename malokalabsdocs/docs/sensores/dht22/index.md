[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# DHT22 (No incluido en el kit)

Sensor de humedad y temperatura (relativa)

## Ejemplo de conexión

![dht22](dht22_bb.png)

## Ejemplo de algoritmo

![dhtt](dht22_luabot.png)

