[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# DHT11

Sensor de humedad y temperatura (relativa)

* Reporta la humedad en valor porcentual [0 - 100] %
* Reporta la temperatura en grados centígrados, el valor reportado no entrega parte decimal.

## Ejemplo de conexión

![dht11](nodemcu_dht11_led_bb.png)

## Ejemplo de algoritmo

![dhtt](dht11_led_sw.jpg)

