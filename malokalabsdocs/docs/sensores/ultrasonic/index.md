[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Ultrasonido HC-SR04

<!-- <iframe width="640" height="480" src="https://www.youtube.com/embed/NQPdouAhN1g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../../offline/video/mediadores/sensor-ultrasonidos.jpg" controls autoplay loop>
  <source src="../../offline/video/mediadores/sensor-ultrasonidos.m4v" type="video/mp4">
</video> 

## Ejemplos

### Conexiones

![conexiones](nodemcu_ultrasonido_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Algoritmo

![algoritmo](ultrasonido.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

