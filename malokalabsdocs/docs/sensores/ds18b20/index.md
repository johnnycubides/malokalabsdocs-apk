[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Sensor de Temperatura DS18B20

## Ejemplo de medición de temperatura del agua

![agua-en-vaso-sensor](vaso_termometro_agua.png)

### Conexión del hardware

Para éste caso además de la tarjeta y su cable de poder (cable USB conectado a un cargador o powerbank)
necesitarás los siguientes materiales:

* Resistencia de 4.7K (Amarillo, Morado, Rojo, Plateado).
* Sensor de temperatura DS18B20.
* Protoboard de cualquier tamaño

!!! Warning
    Recuerda no energizar tu circuito mientras lo estás construyendo.

Con el siguiente diagrama pictográfico haremos las conexiones respectivas,
fíjate bien en la posición de cada elemento, mientras aprendes deberás ser
riguroso en seguirlo al "pie de la letra". 

![Conexión del hardware](ds18B20_bb.png)

!!! Info
    Para el sensor DS18B20 hay al menos dos versiones de conexiones. Te presentamos una conexión alternativa
    en el siguiente diagrama pictográfico, ya que a simple vista es difícil diferencial cual de las versiones
    del sensor tienes a mano. Te invitamos a usarla si con las conexiones del diagrama anterior no lo puedes
    hacer funcionar.

![conexiones de hardware alternativo](ds18B20_v2_bb.png)

<!-- Te presentamos el siguiente vídeo que te dará ideas acerca de lo que debes tener en cuenta para realizar las conexiones -->

<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/MuDFMsN-kLs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

Recuerda que si las terminales de la sonda (ds18b20) no están endurecidas podrás usar alfileres para facilitar
la conexión en la protoboard. Mira la imagen de ejemplo:

![Imagen de ejemplo alfileres](example.jpeg)

###  Algoritmo

Debes fijarte bien en la imagen, las máquinas ejecutan exactamente
lo que se les ordena, si por ejemplo donde dice *en el pin* sino
pones el número 1 y dejas el número 3 la tarjeta de desarrollo no
podrá localizar el sensor. Te recomendamos revisar
varias veces tu programa y ésta imagen.

![Algoritmo](ds18b20-algoritmo.png)

<!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/6Y3XoPVX08Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

### Resultado

Como en casos anteriores, dirígete al navegador que tengas
instalado en un dispositivo (Firefox, Chome) y en el buscador
pon el texto **192.168.4.1** el cual corresponde a la dirección de
la tarjeta de desarrollo y dale a continuación en *ir*,
ésto te llevará a la página Web donde podrás visualizar los datos
del sensor y el botón **Actualizar**; ese botón te permite ordenarle
a la tarjeta que lea los datos del sensor de la temperatura.
Tu resultado es similar al de la siguiente imagen:

![Resultado](ds18b20-firefox.png)

Para limpiar el registro de la temperatura en el sensor 
te recomendamos **oprimir dos** veces el botón *actualizar*,
así tendrás el dato de la temperatura más nuevo.
