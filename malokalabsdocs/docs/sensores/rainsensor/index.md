[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Sensor de lluvia

<!-- <iframe width="640" height="480" src="https://www.youtube.com/embed/0F6Dml0DAd8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../../offline/video/mediadores/sensor-de-agua.jpeg" controls autoplay loop>
  <source src="../../offline/video/mediadores/sensor-de-agua.m4v" type="video/mp4">
</video> 

Este sensor puede ser usado para determinar la cantidad de agua en un recipiente.
Si por ejemplo queremos medir la cantidad de lluvia podemos construir nuestro
propio pluviómetro acondicionando un recipiente para tal propósito y protegiendo
del sensor los elementos electrónicos.

## Ejemplo

### Conexiones

![conexiones](nodemcu_rainSensor_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Algoritmo

![algortimo](lluvia-algoritmo.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

### Prueba del sensor

Como en el reto relacionado a mi primera página web, deberás ver el resultado en el navegador de la tablet o celular.
Sino recuerdas como se realiza esta prueba, repasa el reto relacionado.
