[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Potenciometro

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../../offline/video/mediadores/potenciometro-back.jpg" controls autoplay loop>
  <source src="../../offline/video/mediadores/potenciometro.m4v" type="video/mp4">
</video> 

## Ejemplo 1

### Conexiones

![pot_3leds](nodemcu_pot_3Leds_bb.png)

### Algortimo

![pot-3leds-sw](nodemcu_3leds_sw.png)

