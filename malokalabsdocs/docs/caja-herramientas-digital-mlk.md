# Caja de herramientas digitales de MalokaLabs

## ¿Cómo usar MalokaLabs?

!!! Info "¿Cómo usar MalokaLabs?"
    
    [![](img/banners/MalokaLabs.png){ width="100%" .heartSmall }](tools/catalejo-editor/index.html)

    Te explicamos a continuación ¿Cómo instalar la app de MalokaLabs y como hacer uso de sus funcionalidades principales como son: Compartir proyectos, editar y guardar proyectos

    [:material-cursor-default-click:{.heartBig}
    Explora MalokaLabs
    :material-cursor-default-click:{.heartBig}](tools/catalejo-editor/index.html){ .md-button .md-button--primary}


## Explorando la tablet

!!! Note "Explorando la tablet"

    [![](img/banners/Tablet.png){ width="100%" .heartSmall }](tools/tablet/index.html)

    Te explicamos a continuación todas los componentes que vienen junto a la tableta gráfica.

    [:material-cursor-default-click:{.heartBig}
    Explorar la tablet
    :material-cursor-default-click:{.heartBig}](tools/tablet/index.html){ .md-button .md-button--primary}

## Usar Zoom desde llamada telefónica

!!! Warning "Usar zoom desde llamada telefónica"
    
    [![](img/banners/ZoomAndroid.png){ width="100%" .heartSmall }](tools/zoom/index.html)

    Usar Zoom desde llamada telefónica

    Te vamos a presentar una ayudas que te pueden servir a mejorar tu experiencia en las sesiones sincrónicas en Chicas Steam, comencemos.

    [:material-cursor-default-click:{.heartBig}
    Usar Zoom desde llamada telefónica
    :material-cursor-default-click:{.heartBig}](tools/zoom/index.html){ .md-button .md-button--primary}

## Cacharros que inspiran

!!! Danger "Cacharros que inspiran..."

    [![](img/banners/Cacharros.png){ width="100%" .heartSmall }](cacharros/alarma/index.html)

    A continuación, te mostramos videos que te pueden inspirar en la creación de tus proyectos con el kit de Chicas STEAM.
    
    [:material-cursor-default-click:{.heartBig}
    Ir a cacharros que inspiran...
    :material-cursor-default-click:{.heartBig}](cacharros/alarma/index.html){ .md-button .md-button--primary}

## Reiniciar tarjeta

!!! Danger "Reiniciar tarjeta"
    
    [![](img/banners/Tarjeta.png){ width="100%" .heartSmall }](intro/reinicio/index.html)

    En este apartado encontrarás información relacionada al reinicio de la tarjeta nodemcu, esto puede ser importante cuando tu tarjeta no permita nuevamente la programación desde tu celular.

    [:material-cursor-default-click:{.heartBig}
    Reiniciar tarjeta
    :material-cursor-default-click:{.heartBig}](intro/reinicio/index.html){ .md-button .md-button--primary}

## Documentación del Kit

!!! Info "Documentación del Kit"

    [![](img/banners/documentacion_kit.jpg){ width="100%" .heartSmall }](index-mlk.html)

    Documentación del Kit de Herramientas
    Documentación KiT y Aplicación MalokaLabs
    Guías de uso sobre la caja de herramientas
    Guia de recursos 

    [:material-cursor-default-click:{.heartBig}
    Documentación del Kit
    :material-cursor-default-click:{.heartBig}](index-mlk.html){ .md-button .md-button--primary}
    
## Hola Kit

!!! Note "Hola Kit"

    [![](img/banners/Luabot.png){ width="100%" .heartSmall }](intro/mi-primer-programa/index.html)

    Sigue los pasos que encontrarás en el video para que puedas hacer tu primer programa con la tarjeta y Malokalabs, tendrás las instrucciones e información de los materiales a usar.

    [:material-cursor-default-click:{.heartBig}
    Documentación del Kit
    :material-cursor-default-click:{.heartBig}](index-mlk.html){ .md-button .md-button--primary}

## Discord: Mis primeros pasos

!!! Note "Discord: mis primeros pasos"

    [![](img/banners/Discord.png){ width="99%" .heartSmall }](tools/discord/index.html)

    Chicas STEAM te da la oportunidad de interactuar con una gran comunidad de mediadores, mentoras y con más Chicas STEAM que han hecho o hacen parte del programa a través de chats, canales para vídeo-llamadas y demás contenido de interés que tu podrás alimentar con tus aportes, te invitamos a hacer parte de esta gran comunidad.

    [:material-cursor-default-click:{.heartBig}
    Discord
    :material-cursor-default-click:{.heartBig}](tools/discord/index.html){ .md-button .md-button--primary}

## Moodle MalokaLabs: Plataforma Chicas STEAM

!!! Info "Moodle de MalokaLabs"

    [![](img/banners/MalokaLbas_Moodle.jpg){ width="98%" .heartSmall }](tools/chicas-steam/index.html)

    [:material-cursor-default-click:{.heartBig}
    Plataforma Chicas STEAM
    :material-cursor-default-click:{.heartBig}](tools/chicas-steam/index.html){ .md-button .md-button--primary}

