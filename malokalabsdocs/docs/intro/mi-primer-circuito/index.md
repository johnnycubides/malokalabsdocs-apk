[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Mi primer LED

![mi primer led programado.png](mi-primer-led-programado.png)

## Audio de apoyo sobre el reto

!!! Info "Este reto es muy importante, te invitamos a escuchar las instrucciones en el siguiente audio"
    
    <audio controls style="width: 100%">
      <source src="../../offline/audios/mi-primer-circuito.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>

## Antes de realizar el reto:

!!! Info "Antes de realizar tu primer circuito te invitamos a realizar la exploración del kit, además, para que comprendas como iniciarte con el kit te invitamos a revisar el siguiente vídeo:"

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../../offline/video/mediadores/video-jumpers.jpg" controls loop>
  <source src="../../offline/video/mediadores/video-jumpers.m4v" type="video/mp4">
</video> 

## ¡Hora de realizar el reto!

Realizada la exploración del kit te invitamos a realizar tu "primer circuito", por favor revisa la siguiente presentación donde aprenderás claves para realizar tus circuitos (hardware).

--8<--
primer-circuito-img.md
primer-circuito-pdf.md;
--8<--

<!-- ## Simulación del circuito realizado -->

<!-- A continuación encontrarás la simulación del circuito que acabas de realizar, te invitamos a que modifiques el valor de la resistencia dando en el componente doble clic, analiza que sucede con el LED y la corriente al cambiar aquellos -->
<!-- valores. -->

<!-- <iframe src="/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWKsBsYBMYwp+gjABwroAsAnGSAhEgpFQKYC0eeAUAE4hkDsKIJEgW58qgqODCQ2AD24FhJHuhB5ICJEs2qVAJQYBnAJYGALgwB2AYyMBDNnmIj+YBCt78SjkCoAmDADNbAFcAG1MmUIZfVQl6GEh2OQoXMjxVRMowMhIBHRAAGQBRABE2ADcQHgJckhRc6trveIF6MGgIFpgENgB3KpqBeoHchHFpfsaxXI9pqD7nASFFr34JxdcVKc35ycGdqdX5gHMRuamEFC7ZEBReJa2NJe08FQA5AHt-AFkAYQBVG48e4kdD0AjoTyYPKvEAAHQADnCDGAAGo3AhwIb8Ah4HhDSi5WFMZEAcTYQA" width="800" height="500"></iframe> -->

<!-- [:material-cursor-default-click: Simulación en pantalla completa :material-cursor-default-click:](/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWKsBsYBMYwp+gjABwroAsAnGSAhEgpFQKYC0eeAUAE4hkDsKIJEgW58qgqODCQ2AD24FhJHuhB5ICJEs2qVAJQYBnAJYGALgwB2AYyMBDNnmIj+YBCt78SjkCoAmDADNbAFcAG1MmUIZfVQl6GEh2OQoXMjxVRMowMhIBHRAAGQBRABE2ADcQHgJckhRc6trveIF6MGgIFpgENgB3KpqBeoHchHFpfsaxXI9pqD7nASFFr34JxdcVKc35ycGdqdX5gHMRuamEFC7ZEBReJa2NJe08FQA5AHt-AFkAYQBVG48e4kdD0AjoTyYPKvEAAHQADnCDGAAGo3AhwIb8Ah4HhDSi5WFMZEAcTYQA){ .md-button .md-button--primary } {==Lanza el simulador de circuitos en pantalla completa==} -->

<!-- <iframe src="/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA" width="800" height="500"></iframe> -->

<!-- ## Herramientas -->

<!-- * [Calculadora de resistencias de carbón 4 bandas](https://www.digikey.com/es/resources/conversion-calculators/conversion-calculator-resistor-color-code) -->

<!-- ## Es hora de comprender -->

<!-- ### ¿Qué son los LEDs? -->

--8<--
leds-jpg.md;
leds-pdf.md;
--8<--

<!-- ### ¿Qué son las resistencias? -->

--8<--
resistencia-jpg.md;
resistencia-pdf.md;
--8<--

<!-- ### ¿Cómo usar la protoboard? -->

<!-- Debe insertar un ../ extra en el path si el archivo no es un index.md, el cual ya se ha incorporado en el path del data -->
<!-- <object -->
<!--   width="100%" -->
<!--   height="600" -->
<!--   type="application/pdf" -->
<!--   data="protoboard.pdf"> -->
<!-- </object> -->

<!-- ### Componentes de un circuito eléctrico -->

<!-- Debe insertar un ../ extra en el path si el archivo no es un index.md, el cual ya se ha incorporado en el path del data -->
<!-- <object -->
<!--   width="100%" -->
<!--   height="600" -->
<!--   type="application/pdf" -->
<!--   data="../kit/Elementos de circuitos.pdf"> -->
<!-- </object> -->

## Sugerencias

Te invitamos a revisar la biblioteca electrónica para que puedas comprender temas relacionados a:

[:material-cursor-default-click:
¿Qué son las resistencias?
:material-cursor-default-click:](../../biblioteca-electronica/resistencia/index.html){ .md-button .md-button--primary .heart}

[:material-cursor-default-click:
¿Qué es la protoboard?
:material-cursor-default-click:](../../biblioteca-electronica/protoboard/index.html){ .md-button .md-button--primary .heart}

[:material-cursor-default-click:
¿Qué son los LEDs?
:material-cursor-default-click:](../../biblioteca-electronica/leds/index.html){ .md-button .md-button--primary .heart}

[:material-cursor-default-click:
Elementos de circuitos eléctricos
:material-cursor-default-click:](../../biblioteca-electronica/elementos-circuitos/index.html){ .md-button .md-button--primary .heart}


