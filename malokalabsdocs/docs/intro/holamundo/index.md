[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Hola Mundo

![mi primer led programado.png](mi-primer-led-programado.png)

## Audio de apoyo sobre el reto

!!! Info "Escucha las recomendaciones dadas para este reto en particular en el siguiente audio"
    
    <audio controls style="width: 100%">
      <source src="../../offline/audios/mi-primer-led-programado.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>
    

## Antes de realizar el reto:

!!! Info "Antes de realizar este ejemplo deberás realizar tu [primer programa con Catalejo Editor/MalokaLabs, visita este enlace](../../mi-primer-programa)"

El hola mundo es el primer acercamiento que tienes con la tarjeta
de desarrollo y con el software de programación. A continuación
encontrarás imágenes y un vídeo que te puede orientar sobre cómo
encender tu primer LED. ¡Disfruta del contenido!.

## Materiales

Para desarrollar éste ejemplo necesitarás:

|Cantidad|Nombre|Imagen|
|:-------------:|:-------------:|:-----:|
|1 | [Tarjeta NodeMCU](../../../nodemcu_v1/) con el [software instalado](../../../install/firmware/)| ![Imagen nodemcu v3](../../img/esp8266/nodemcu.png){: style="width:30%; background:white;"}|
|1| Resistencia de 330 Ohms | ![resistencia 220](../../img/basic/resistencia330.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| LED de cualquier color| ![led](../../img/actuadores/led-difuso.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}
|1| Conector rápido (Jumper) macho-macho| ![conector rápido m-m](../../img/basic/jumper-m-m.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1| Tablero de prototipos (Protoboard)| ![protoboard](../../img/basic/protoboardHalf.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|
|1 | Cable USB micro B| ![cable USB](../../img/accesories/usb-micro-b.jpg){: style="width:30%;"}|
|1 | Cargador de celular o [baterías](../../../intro/energize-card/) (PowerBank) | ![cargador de celular](../../img/accesories/charger-5v.png){: style="width:30%; margin-left: auto; margin-right: auto; display: block"}|

## Creando nuestro circuito

Lo primero que debes hacer es imitar de la siguiente imagen la
manera y los lugares donde se conecta cada elemento del circuito
a armar, comprobando las veces que sea necesario nuestro montaje
con el diagrama pictografico, ¡Manos a la obra!

![Ejemplos de conexión](nodemcu_led_v2_bb.png)

Si requieres apoyo en el montaje de este circuito puedes guiarte a través del siguiente vídeo.

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="" controls autoplay loop>
  <source src="../../offline/retos/led-circuito.m4v" type="video/mp4">
</video> 

## Construyamos nuestro programa

Abre la aplicación *Catalejo Editor/MalokaLabs* juega un poco con ella
buscando los bloques de programación de código y cuando la hayas explorado
lo suficiente trata de armar el siguiente código de bloques,
revisa muchas veces que te quede igual.

![Ejemplo de programa](mi-primer-led-catalejo.png)

Si requieres apoyo para ubicar los bloques para la programación en tu aplicación te inviatmos a ver el siguiente video.

<video style="max-width: 100%; width:100%; height: auto; border: 2px solid gray;" poster="" controls autoplay loop>
  <source src="../../offline/retos/mi-primer-led-programado.m4v" type="video/mp4">
</video> 

Desde luego hay detalles que se te escaparán y muchas dudas
tendrás; ten presente que es difícil representar todas las
respuestas en ese par de imágenes, para ello te invitamos
a ver el siguiente vídeo que despejará unas dudas y desde
luego generará otras. No olvides que puedes usar nuestro
servidor de 
[Discord](https://discord.gg/4GxYxyy)
o
[nuestro foro](https://catalejo.flarum.cloud/t/catalejo-luabot-nodemcu)
para hacer esas preguntas que te quedan, estaremos prestos a apoyarte.

## Probando el programa en el montaje

Cuando hayas enviado el programa deberás oprimir el botón **RST** y verificar el funcionamiento del LED.

!!! Info "Si tu circuito no funciona, deberás verificar nuevamente los pasos para detectar un posible error en el montaje del circuito, en el programa que creaste con MalokaLabs o en el ejercicio de probar el programa."
    


<!-- ## Vídeo que te responderá algunas dudas -->


<!-- <iframe width="640" height="480" src="https://www.youtube.com/embed/WT7xEaLsfng" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

