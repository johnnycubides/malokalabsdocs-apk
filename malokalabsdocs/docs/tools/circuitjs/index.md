# CircuitJS

## Simulador

<!-- <iframe src="/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA" width="800" height="500"></iframe> -->

## Simulador pantalla completa

<!-- [:material-cursor-default-click: CircuitJs en pantalla completa :material-cursor-default-click:](/android_asset/malokadocs/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA"){ .md-button .md-button--primary } {==Lanza el simulador de circuitos en pantalla completa==} -->

<!-- [:material-cursor-default-click: CircuitJs en pantalla completa :material-cursor-default-click:](/circuitjs/circuitjs.html?ctz=CQAgzCAMB0l3BWEBGaCBsAWATOsyAOAzQg5AdmxAQE5rrJqBTAWmWQCgAnEG89EJkwFe-akKjgwkDgA9eRQZRSQESTOXUoqAJSYBnAJb6ALkwB2AY0MBDDsnRU+AsAidiSAqgBMmAMxsAVwAbExZgpm8USUYYSE55GhoXGmQVZDowGkxBbRAAGQBRABEOADcQcmJBLErqzxjBRjBoCFioNA4AdzqczFqqnIQJGR7B8RznCahu0QEhESmG0bnwN161qhXx1ypx5Y4Acw3hnPGMdpl5dD5BHEq1O61kKgA5AHtfAFkAYQBVOSVW44RgEXCCbAQHIvEAAHQADrD9GAAGqAghwGoCMjkGp0aFUFhIgDiHCAA"){ .md-button .md-button--primary } {==Lanza el simulador de circuitos en pantalla completa==} -->

## Documentación sobre el simulador

<!-- Debe insertar un ../ extra en el path si el archivo no es un index.md, el cual ya se ha incorporado en el path del data -->
<!-- <object -->
<!--   width="100%" -->
<!--   height="600" -->
<!--   type="application/pdf" -->
<!--   data="./circuitjs-doc-es.pdf"> -->
<!-- </object> -->
