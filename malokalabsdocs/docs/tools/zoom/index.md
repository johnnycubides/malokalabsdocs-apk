[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Zoom

## Unirse a un encuentro de zoom desde una llamada telefónica

Para que más chicas sin acceso a Internet pero con línea telefónica puedan unirse a los encuentros virtuales te presentamos la siguiente opción (revisa los pasos):

![zoom desde llamada telefónica](zoom-llamada-telefono.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

Las indicaciones para el ingreso son las siguientes: 

1. Tener recarga de minutos 📱

2. Hacer una llamada telefónica a cualquiera de estos números: 
> 601 508 77 02 - 602 620 73 88 📞

3. Ingresar el ID (número de identificación de cada reunión) y finalizar con el símbolo #. El ID es un código que se encuentra en cada uno de los enlaces de las reuniones o talleres, recuerda que cada invitación tiene un ID diferente por ejemplo en el enlace: 
![ejemplo de id de zoom](zoom-id-sample.png)
>El ID es: **82251309481**
4. Luego de ingresar el ID y el símbolo # la contestadora les pedirá una información adicional (ID de Participante) en este caso omitimos esta información y digitamos nuevamente el símbolo #

5. Una vez la chica ingresa al encuentro a través de la línea telefónica le pedimos que se presente indicando su nombre completo con la finalidad de registrar su participación. Para levantar la mano la niña debe digital \*9 y para que la podamos escuchar o silenciar debe digitar \*6
