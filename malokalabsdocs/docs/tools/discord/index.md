[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Servidor de Discord de Chicas STEAM

![Servidor de discord](Discord.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

Chicas STEAM te da la oportunidad de interactuar con una gran comunidad de mediadores, mentoras y con más Chicas STEAM que han hecho o hacen
parte del programa a través de chats, canales para vídeo-llamadas y demás contenido de interés que tu podrás alimentar con tus aportes,
te invitamos a hacer parte de esta gran comunidad.

## Invitación al servidor de Discord de Chicas STEAM

Visita el siguiente enlace

[https://discord.gg/dgpH6ek8gK](https://discord.gg/dgpH6ek8gK)

## ¿No sabes nada del mundo de Discord y quieres aprender?

Revisa la siguiente información que te servirá para aprender a usar Discord.

<!-- Debe insertar un ../ extra en el path para que funcione, el cual ya se ha incorporado en el path del data -->
<!-- <object -->
<!--   width="100%" -->
<!--   height="600" -->
<!--   type="application/pdf" -->
<!--   data="Tutorial discord.pdf"> -->
<!-- </object> -->

# Vídeos de apoyo para aprender a usar Discord

Puedes apoyarte en los siguientes vídeos que te permitirán seguir el paso a paso de todo lo que necesitas para mejorar tu experiencia en Discord.

[Vídeos de uso de discord](https://maloka.gitlab.io/malokalabsdocs/malokalabs-cards/posts/use-discord/)
