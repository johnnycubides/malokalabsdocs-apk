[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Moodle Chicas STEAM

![malokalabs](malokalabsModle.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

## Enlace a la plataforma de moodle

[https://steamnacional.malokalabs.org](https://steamnacional.malokalabs.org)

## Tutorial de uso de la plataforma de moodle de MalokaLabs

<!-- Debe insertar un ../ extra en el path para que funcione, el cual ya se ha incorporado en el path del data -->
<!-- <object -->
<!--   width="100%" -->
<!--   height="600" -->
<!--   type="application/pdf" -->
<!--   data="MalokaNacional.pdf"> -->
<!-- </object> -->
