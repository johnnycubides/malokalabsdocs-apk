[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# MalokaLabs/Catalejo Editor

![Malokalabs](malokalabs.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}


Te explicamos a continuación, cómo instalar la
app de MalokaLabs y hacer uso de sus
funcionalidades principales como son:

* Compartir proyectos
* Editar programas
* guardar proyectos

## Instalación de MalokaLabs

Te recordamos que la app de **MalokaLABS ya está instalada en la tablet que recibiste**, sin embargo,
si quieres instalarla en otro dispositivo puedes seguir los pasos del siguiente vídeo.

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="malokalabs.png" controls autoplay loop>
  <source src="../../offline/video/tools/instalacion-de-malokaLlabs-desde-android.mp4" type="video/mp4">
</video> 

## Crear código con Blockly

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="malokalabs.png" controls autoplay loop>
  <source src="../../offline/video/tools/crear-codigo-con-blockly.mp4" type="video/mp4">
</video>

## Abrir y guardar proyectos

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="malokalabs.png" controls autoplay loop>
  <source src="../../offline/video/tools/abrir-y-guardar-proyectos.mp4" type="video/mp4">
</video> 

## Compartir proyectos

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="malokalabs.png" controls autoplay loop>
  <source src="../../offline/video/tools/compartir-proyectos.mp4" type="video/mp4">
</video> 

## Conexión con tarjeta

Para hacer este ejercicio deberás primero conectar la mini-computadora NodeMCU a un cargador USB para
encenderla y generar la red WiFi Catalejo-xxxx.

![montaje del circuito](../../intro/mi-primer-programa/tarjeta-energizada-con-indicaciones-sobre-rst.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="malokalabs.png" controls autoplay loop>
  <source src="../../offline/video/tools/conexion-con-tarjeta.mp4" type="video/mp4">
</video> 

!!! Info "Recuerda que la contraseña de la red WiFi de Catalejo es 87654321"
    

## Proceso de exploración de documentación

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="malokalabs.png" controls autoplay loop>
  <source src="../../offline/video/tools/proceso-de-exploracion-de-documentacion.mp4" type="video/mp4">
</video> 
