[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Tablet

![Exploración de la tablet](Tablet.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

## Exploración de la tablet Touch+ 770G

![tablet 770G](touch770g-removebg-preview.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

| Caracteríticas:              |  |
| ------------------- | ------------ |
| Marca         | TOUCH+      |
| Modelo        | 770G        |
| Pantalla | 7 Pulgadas |
| Resolución pantalla | 600 x 1024 |
| Memoria RAM         | 2 GB         |
| Almacenamiento      | 16 GB        |
| CPU | 4x ARM Cortex-A53 @ 1400 MHz |

**A continuación te compartimos el siguiente vídeo para que puedas realizar una exploración guiada de la tablet.**

<video style="max-width: 100%; width: 100%; height: auto; border: 2px solid gray;" poster="touch770g-removebg-preview.png" controls autoplay loop>
  <source src="../../offline/video/tools/exploracion-de-la-tablet-y-sus-demas-componentes.m4v" type="video/mp4">
</video> 

## Reinicio general de la tablet

El *reinicio general de la tablet* es útil cuando la tablet por alguna razón se bloquea y no quiere recibir ordenes o cuando se quiere realizar un reinicio con los parámetros de fábrica.

<video style="max-width: 100%; width: 100%; height: auto; border: 2px solid gray;" poster="" controls autoplay loop>
  <source src="../../offline/video/tools/reinicio-general-de-la-tablet.mp4" type="video/mp4">
</video> 

## Activación de credenciales Moodle de Chicas STEAM en la tablet

<video style="max-width: 100%; width: 100%; height: auto; border: 2px solid gray;" poster="" controls autoplay loop>
  <source src="../../offline/video/tools/activacion-de-credenciales-en-la-plataforma-moodle-de-malokalabs-y-chicasteam.mp4" type="video/mp4">
</video> 
