# Calculador de valor de resistencia

## Calculadora

<!-- <iframe src="resistor/index.html" width="100%" height="500"></iframe> -->
<!-- <iframe src="/android_asset/malokadocs/resistor/index.html" width="100%" height="500"></iframe> -->

## Calculadora en pantalla completa

<!-- [:material-cursor-default-click: Calculadora de resistencia en pantalla completa :material-cursor-default-click:](/android_asset/malokadocs/resistor/index.html){ .md-button .md-button--primary } {==Lanza la calculadora en pantalla completa==} -->

## ¿Qué es una resistencia?

<!-- Debe insertar un ../ extra en el path si el archivo no es un index.md, el cual ya se ha incorporado en el path del data -->
<!-- <object -->
<!--   width="100%" -->
<!--   height="600" -->
<!--   type="application/pdf" -->
<!--   data="que_es_una_resistencia_y_la_potencia.pdf"> -->
<!-- </object> -->
