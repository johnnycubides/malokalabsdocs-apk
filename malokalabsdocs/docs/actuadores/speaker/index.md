[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# Parlante

* El sonido puede ser generado por hardware con una frecuencia entre 0 Hz hasta 1000 Hertz
* Si se desea obtener frecuencias superiores se podría estables por software, sin embargo no es recomendable
* El volumen del sonido puede ser logrado variando el ciclo útil entre 0 a 512.

## Ejemplos

### Circuito de señal de audio y luz

<!-- <iframe width="640" height="480" src="https://www.youtube.com/embed/Gg459V0I1HA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../../offline/video/mediadores/speaker.jpg" controls autoplay loop>
  <source src="../../offline/video/mediadores/parlante.m4v" type="video/mp4">
</video> 

### El transistor como amplificador de audio

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../../offline/video/mediadores/parlantes.jpg" controls autoplay loop>
  <source src="../../offline/video/mediadores/transistor-montaje.m4v" type="video/mp4">
</video> 

### Sonido ON/OFF

En este ejemplo se generará un sonido a una frecuencia y volumen fijo, notaremos como en cada segundo que pase
se presentará un sonido y luego un silencio; esto se repetirá por 4 veces para luego quedar en silencio.

#### Conexiones

![parlante pictografico](nodemcu_parlante_bb.png){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}

#### Algoritmo

![parlante algoritmo](parlante-algoritmo.jpeg){: style="width:100%; margin-left: auto; margin-right: auto; display: block"}


