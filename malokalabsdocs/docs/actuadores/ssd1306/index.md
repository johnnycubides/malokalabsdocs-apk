[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

# SSD1306 oled 128x64 i2c (No incluido en el kit)

## Ejemplo de conexión

![Esquema de conexión](nodemcu_oled_i2c_128x64_bb.png)

## Ejemplo de algoritmo

### Pintando texto

![pintar texto](display_texto.jpg)

### Limpiando pantalla

![Limpiando pantalla](display_limpiar_pantalla.jpg)

### Pintando líneas

![Pintando líneas](display_linea.jpg)

### Pintando círculos y discos

![pintando círculos y discos](display_circulo.jpg)

### Pintando marcos y cajas

![Pintando marcos y cajas](display_marco.jpg)

