[:material-arrow-left: Regresar al menú](index.html){.md-button}

# Kit MalokaLabs (basado Catalejo editor)

![ejemplos de proyectos](./img/index/presentation1.png)

!!! Info "Escucha el audio de bienvenida a la documantación del Kit"
     <audio controls style="width: 100%">
      <source src="offline/audios/audio-introduccion.ogg" type="audio/ogg">
      Your browser does not support the audio element.
    </audio>

:material-spider:**¡Un gran poder conlleva una gran responsabilidad!**:material-spider:

Bienvenida al mundo maker/hacker: curiosea, pregunta, imagina, construye y sueña con este material diseñado para ti.

## ¿Cómo iniciarte? :material-face-woman-shimmer:

!!! Info "Escucha el audio para indicarte como inciarte con el Kit"

    <audio controls style="width: 100%">
      <source src="offline/audios/como-iniciarte.ogg" type="audio/ogg" >
      Your browser does not support the audio element.
    </audio> 

Realiza la exploración del kit y luego ponte aprueba con los retos que tenemos para ti.

### Exploración del Kit :material-human-female-dance:

Para iniciarte en este mundo de makers lo que primero deberás hacer es conocer los componentes que tiene tu kit, para tal fin te invitamos a zir al siguiente enlace realizando los
ejercicios propuestos en el primer vídeo como leyendo la descripción de cada elemento del kit.

[:material-tools: Enlace para explorar el Kit :material-tools:](intro/kit/index.html){ .md-button .md-button--primary .heart}

### Retos :material-sort-numeric-ascending:

!!! Info "Te invitamos a realizar los siguientes retos que están organizados desde el nivel 0 hasta el nivel 3; comparte tu experiencia en el servidor de Discord"
    Al realizar los retos desde el nivel 0 al nivel 3 podrás:

    - [x] Reconocer la polaridad de los componentes eléctricos
    - [x] Podrás construir circuitos eléctricos (Hardware)
    - [x] Crear y enviar programas a la minicomputadora nodemcu (Software)
    - [x] Controlar tus circuitos eléctricos a través de una página Web
        


[Reto nivel 0 :material-star-shooting:{.heartBig}](intro/mi-primer-circuito/index.html){ .md-button .md-button--primary} {==Construye tu primer circuito==}

[Reto nivel 1 :material-fire:{.heartBig}](intro/mi-primer-programa/index.html){ .md-button .md-button--primary} {==Atrévete a crear tu primer programa con el kit==}

[Reto nivel 2 :material-fire:{.heartBig}:material-fire:{.heartBig}](intro/holamundo/index.html){ .md-button .md-button--primary} {==Programa tu primer LED con CatalejoEditor/MalokaLabs==}

[Reto nivel 3 :material-fire:{.heartBig}:material-fire:{.heartBig}:material-fire:{.heartBig}](intro/holamundo-http/index.html){ .md-button .md-button--primary} {==Crea tu primera página Web y controla tu kit==}


## Nuestra comunidad

Ésta documentación está en continua construcción te invitamos a participar compartiendo tu
experiencia desarrollando tus proyectos. 

Te invitamos a ser parte de nuestro servidor en discord, dale clic en la imagen:


[:fontawesome-brands-discord: :material-telescope: Catalejo :material-telescope: :fontawesome-brands-discord:](https://discord.gg/4GxYxyy){ .md-button .md-button--primary .heart}

[Enlace de invitación servidor de discord Catalejo](https://discord.gg/4GxYxyy)

[:fontawesome-brands-discord: Chicas STEAM :fontawesome-brands-discord:](https://discord.gg/dgpH6ek8gK){ .md-button .md-button--primary .heart}

[Enlace de invitación servidor de discord Chicas STEAM](https://discord.gg/dgpH6ek8gK)

¡Estaremos felices de leerte!

## Agradecimientos

Agradecemos la colaboración de cada estudiante que ha aportado su grano de arena
para ayudarnos a generar una documentación a ésta valiosa herramienta que hemos denominado
LuaBot.

Atentamente:

<img src="johnny.jpeg" style="width:25%;">
<p>
Johnny Cubides<br>
e-mail: jgcubidesc@unal.edu.co<br>
discord: johnnycubides#0705<br>
telegram: @johnny5<br>
<p>
