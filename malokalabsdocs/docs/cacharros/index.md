[:material-arrow-left: Regresar al menú](../../index.html){.md-button}

![](Cacharros.png){ width="100%" }

# Cacharros que inspiran

## Vídeos explicativos sobre cacharros

### Protoboards y cables jumpers

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/video-jumpers.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/video-jumpers.m4v" type="video/mp4">
</video> 

### El mundo y los LEDs

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/led-back.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/led.m4v" type="video/mp4">
</video> 

### Los pulsadores en la vida cotidiana

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/pulsadores-back.png" controls autoplay loop>
  <source src="../offline/video/mediadores/pulsadores.m4v" type="video/mp4">
</video> 

### ¿Qué onda con los potenciómetros?

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/potenciometro-back.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/potenciometro.m4v" type="video/mp4">
</video> 

### El transistor como amplificador de audio

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/transistor-back.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/transistor.m4v" type="video/mp4">
</video> 

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/parlantes.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/transistor-montaje.m4v" type="video/mp4">
</video> 

### Sensor de agua lluvia

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/sensor-de-agua.jpeg" controls autoplay loop>
  <source src="../offline/video/mediadores/sensor-de-agua.m4v" type="video/mp4">
</video> 

### Sensor ultrasonido

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/sensor-ultrasonidos.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/sensor-ultrasonidos.m4v" type="video/mp4">
</video> 

### Circuito de señal de audio y luz

<video style="max-width: 100%; height: auto; border: 2px solid gray;" poster="../offline/video/mediadores/speaker.jpg" controls autoplay loop>
  <source src="../offline/video/mediadores/parlante.m4v" type="video/mp4">
</video> 


## Guías de construcción

A continuación encontrarás enlaces a distintos cacharros que podrías realizar con tu kit de Chicas STEAM.

### Alarma Sonora

{==

Construye una alarma que cuando detecta la presencia de alguien cercano genera un sonido de alerta

==}

[:material-cursor-default-click:{.heartBig}
Alarma sonora
:material-cursor-default-click:{.heartBig}](alarma/index.html){ .md-button .md-button--primary}

### Bastón Ultrasonido

{==

Se trata de la construcción de un bastón que permite escuchar la cercanía de objetos;
está pensado para localizar obstáculos al caminar sin hacer uso de la capacidad visual.
Puede pensarse como una herramienta útil para aquellas personas que tengan una condición
de discapacidad visual.

==}

[:material-cursor-default-click:{.heartBig}
Bastón ultrasonido
:material-cursor-default-click:{.heartBig}](baston_ultrasonido/index.html){ .md-button .md-button--primary}

### Bufanda Luminosa

{==

Te mostraremos indicaciones para que puedas tejer tu propia bufanda con aguja y lana. Cuando ya la tengas lista
te explicaremos como agregar luces que puedes poner a iluminar en diferentes secuencias.

==}

[:material-cursor-default-click:{.heartBig}
Bufanda Luminosa
:material-cursor-default-click:{.heartBig}](bufanda/index.html){ .md-button .md-button--primary}

### Código Morse

{==

Aprende a enviar señales en el código morse (puntos y rayas) a través de luz, este cacharro fue creado por
una chica STEAM como tú.

==}

[:material-cursor-default-click:{.heartBig}
Código Morse
:material-cursor-default-click:{.heartBig}](codigo_morse/index.html){ .md-button .md-button--primary}

### Detector de Sed para Plantas

{==

Construye un artefacto que ter permitirá saber cuándo tu planta tiene sed, tu planta te lo dirá a través de dos emoticones,
uno con una carita feliz y otro con una carita triste

==}

[:material-cursor-default-click:{.heartBig}
Detector de sed opción 1
:material-cursor-default-click:{.heartBig}](detector_sed_1/index.html){ .md-button .md-button--primary}


[:material-cursor-default-click:{.heartBig}
Detector de sed opción 2
:material-cursor-default-click:{.heartBig}](detector_sed/index.html){ .md-button .md-button--primary}

### Hakeando mi Juguete

{==

Podrás crear o modificar un peluche que tengas para agregarle luces, sonidos y movimiento.

==}

[:material-cursor-default-click:{.heartBig}
Hackeando mi Juguete
:material-cursor-default-click:{.heartBig}](hackeando_juguete/index.html){ .md-button .md-button--primary}

### Huertas y Humedad del Suelo

{==

Aprenderás temas relacionados sobre huertas y a cuidar tus plantas para poderlas regar cuando sea necesario.

==}

[:material-cursor-default-click:{.heartBig}
Huertas y Humedad del Suelo
:material-cursor-default-click:{.heartBig}](humedad_suelo/index.html){ .md-button .md-button--primary}

### Reloj Binario

{==

Haciendo tu reloj binario aprenderás sobre cómo las computadoras hacen cuentas y su lenguaje binario.

==}

[:material-cursor-default-click:{.heartBig}
Reloj Binario
:material-cursor-default-click:{.heartBig}](reloj_binario/index.html){ .md-button .md-button--primary}

### Semáforo

{==

¿Te has preguntado cómo los semáforos de transito funcionan? Ésta es tu oportunidad de construir uno.

==}

[:material-cursor-default-click:{.heartBig}
Semáforo
:material-cursor-default-click:{.heartBig}](semaforo/index.html){ .md-button .md-button--primary}

### Theremin

{==

Construye el único instrumento musical que se "toca" sin ser tocado.

==}

[:material-cursor-default-click:{.heartBig}
Theremin
:material-cursor-default-click:{.heartBig}](theremin/index.html){ .md-button .md-button--primary}

