# MalokaLabs

![malokalabs](./img/malokalabs.png)

Te explicamos a continuación ¿Cómo instalar la
app de MalokaLabs y como hacer uso de sus
funcionalidades principales como son: Compartir
proyectos, editar y guardar proyectos

## Instalación de MalokaLabs

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/instalacion-de-malokaLlabs-desde-android.mp4" type="video/mp4">
</video>

## Activación de credenciales en la plataforma Moodle de MalokaLABS y ChicaSTEAM

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/activacion-de-credenciales-en-la-plataforma-moodle-de-malokalabs-y-chicasteam..mp4" type="video/mp4">
</video> 

## Proceso de exploración de documentación

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/proceso-de-exploracion-de-documentacion.mp4" type="video/mp4">
</video> 

## Crear código con Blockly

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/crear-codigo-con-blockly.mp4" type="video/mp4">
</video> 

## Abrir y guardar proyectos

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/abrir-y-guardar-proyectos.mp4" type="video/mp4">
</video> 

## Compartir proyectos

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/compartir-proyectos.mp4" type="video/mp4">
</video> 

## Conexión con tarjeta

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/conexion-con-tarjeta.mp4" type="video/mp4">
</video> 


