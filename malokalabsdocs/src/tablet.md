# Explorando la tablet

![Explorando la tablet](./img/Tablet.png)

Te explicamos a continuación todas los componentes que vienen junto a la tableta gráfica.

## Exploración de tablet y sus demás componentes

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/exploracion-de-la-tablet-y-sus-demas-componentes.m4v" type="video/mp4">
</video> 

## Reinicio general de la tablet

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/reinicio-general-de-la-tablet.mp4" type="video/mp4">
</video> 

