# Hola kit

![hello kit](./img/Luabot.png)

Sigue los pasos que encontrarás en el video para que puedas hacer tu primer programa con la tarjeta y Malokalabs, tendrás las instrucciones e información de los materiales a usar.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/hola-mundo-malokalabs.mp4" type="video/mp4">
</video> 

