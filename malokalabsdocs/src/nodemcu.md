# Tarjeta de desarrollo

![Tarjeta de desarrollo nodemcu](./img/nodemcu-v3.png)

## Reinicio general

![reinicio general](./img/Tarjeta.png)

En este apartado encontrarás información relacionada al reinicio de la tarjeta nodemcu, esto puede ser importante cuando tu tarjeta no permita nuevamente la programación desde tu celular.

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/tools/reinicio-general-de-la-tarjeta-nodemcu.mp4" type="video/mp4">
</video> 

> Si no funciona el reinicio general de la tarjeta, en vez de usar la resistencia prueba con un cable macho macho e introdúcelo en la protoboard
> en el mismo lugar donde viene la resistencia (puedes retirar la resistencia)

![reinicio-cable-macho-macho](./img/reiniciogeneral_bb.jpg)
