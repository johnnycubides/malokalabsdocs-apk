# Proyectos con el kit

## Conectores rápidos (Jumpers)

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/video-jumpers.m4v" type="video/mp4">
</video> 

## El transistor

### ¿Qué es el transistor?

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/transistor.m4v" type="video/mp4">
</video>

### Montaje del transistor con el kit

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/transistor-montaje.m4v" type="video/mp4">
</video> 

## Sensor de agua

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/sensor-de-agua.m4v" type="video/mp4">
</video>

## Fotoresistencia

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/fotorresistencia.m4v" type="video/mp4">
</video>

## LED

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/led.m4v" type="video/mp4">
</video>

## Pulsador

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/pulsador.m4v" type="video/mp4">
</video>

## Potenciómetro

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/potenciometro.m4v" type="video/mp4">
</video> 

## Sensor ultrasonido

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/sensor-ultrasonidos.m4v" type="video/mp4">
</video> 

## Parlante

<video style="max-width: 100%; height: auto;" controls >
    <source src="./video/mediadores/parlante.m4v" type="video/mp4">
</video> 
