package com.malokalabs.cajadeherramientas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gurudev.fullscreenvideowebview.FullScreenVideoWebView;

public class MainActivity extends AppCompatActivity {

    // WebView malokalabs_docs;

    // String url = "file:///android_asset/malokadocs/index.html";

    // @Override
    // protected void onCreate(Bundle savedInstanceState) {
    //     // https://developer.android.com/guide/topics/resources/runtime-changes?hl=es-419
    //     // Lo anterior para que al cambiar la orientación de la tablet no reinicie el wevbiew
    //     // esto se hace en el archivo AndroidManifest.xml
    //     super.onCreate(savedInstanceState);
    //     setContentView(R.layout.activity_main);
    //     malokalabs_docs = (WebView)findViewById(R.id.malokalabs_docs_webview);

    //     /* Agregar características de ZOOM */
    //     malokalabs_docs.getSettings().setBuiltInZoomControls(true);
    //     malokalabs_docs.getSettings().setSupportZoom(true);

    //     final WebSettings ajustesVisorWeb = malokalabs_docs.getSettings();
    //     ajustesVisorWeb.setJavaScriptEnabled(true);

    //     malokalabs_docs.setWebViewClient(new WebViewClient() {
    //       @Override
    //       public boolean shouldOverrideUrlLoading(WebView view, String url) {
    //         view.loadUrl(url); // url del visor actualizable,
    //         // view.loadUrl(url+"index.html"); // url del visor actualizable,
    //         // en esye caso se agrega index.html porque el enrutador de mkdocs no injecta la página específicamente
    //         return true;
    //       }
    //     });

    //     malokalabs_docs.loadUrl(url);
    //     // malokalabs_docs.loadUrl(data.getUrlDocs()); // link de enlace a cargar al arrancar la actividad

    // }

    String url = "file:///android_asset/malokadocs/index.html";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        FullScreenVideoWebView fullScreenVideoWebView = findViewById(R.id.malokalabs_docs);
        /* Agregar características de ZOOM */
        fullScreenVideoWebView.getSettings().setBuiltInZoomControls(true);
        fullScreenVideoWebView.getSettings().setSupportZoom(true);
        fullScreenVideoWebView.loadUrl(url);
    }
}
